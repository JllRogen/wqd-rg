
# 安装依赖 
```sh
git clone git@gitlab.com:JllRogen/wqd-rg.git --recurse-submodules
```


@npm/package-name 这种形式的包名，是有作用域的包名形式，执行 npm publish 的时候默认是发布私有的包。因此，第一种方式是花钱买私有包的服务，另外一种方式就是指定参数，表示公开：

```sh
# npm的发包
npm publish --access public


npm unpublish
```
