const inProgerss = Object.create(null)

/**
 * 加载script
 * @param {*} url
 */
export function scriptLoad(url) {
  if (inProgerss[url]) return inProgerss[url]
  const promise = new Promise((resolve, reject) => {
    const script = createScript(url)

    script.addEventListener('error', function () {
      document.head.removeChild(script)
      reject(Error(`请求失败,${url}`))
    })
    script.addEventListener('load', function () {
      document.head.removeChild(script)
      resolve()
    })
    document.head.appendChild(script)
  })
  inProgerss[url] = promise
  return promise
}

/**
 * 加载script 并返回结果 true false
 * @param {*} url
 */
export function scriptLoadWithResult(url) {
  return scriptLoad(url)
    .then(() => true)
    .catch((err) => {
      console.log(err)
      return false
    })
}

// ins
function createScript(url) {
  const script = document.createElement('script')
  script.async = true
  // script.timeout = 10

  // Only add cross origin for actual cross origin
  // this is because Safari triggers for all
  // - https://bugs.webkit.org/show_bug.cgi?id=171566
  // if (url.indexOf(baseOrigin + '/')) script.crossOrigin = 'anonymous'
  // const integrity = importMap.integrity[url]
  // if (integrity) script.integrity = integrity
  script.src = url
  return script
}
